
var PEDIDO_ID = null;
var MESA = null;
var base_url = null;

function abrirMesa(tag) {
    if(confirm("Deseja realmente abrir esta mesa?")) {

        var mesa_id = tag.getAttribute('mesa_id');
        var nome_mesa = 'Mesa '+tag.getAttribute('mesa_nome');

        var pedido_save = ['', mesa_id ,nome_mesa];

        db_pedidos.save(banco.getBancoDados(), pedido_save);

        $.mobile.changePage("#categorias");
    }
}

function adicionarItemPedido(tag) {

    if (confirm("Adicionar item ao pedido?")) {

        var quantidadeDigitada = prompt("Informe a quantidade do item, ou OK para continuar", '1');

        var observacao = prompt("Informe a observação do item, ou OK para continuar", '');

        if (quantidadeDigitada != ''){
            if (!isNaN(quantidadeDigitada)) {
                quantidadeDigitada = quantidadeDigitada;
            }
        }

        var nome_produto  = tag.getAttribute('nome_produto');
        var valor_produto = tag.getAttribute('valor_produto');
        var produto_id    = tag.getAttribute('produto_id');

        var pedido_item_save = ['', PEDIDO_ID, produto_id, nome_produto , quantidadeDigitada, valor_produto, observacao];

        db_pedido_item.save(banco.getBancoDados(), pedido_item_save);

    }
}

function cancelar() {
    if(confirm("Deseja realmente cancelar o pedido?")) {
        $.mobile.changePage("#mesas");
    }
}

function enviar() {
    db_pedidos.enviar(banco.getBancoDados());
}