var mesas_all = null;

var db_mesas = {

    nm_table: function () {
        return "mesas";
    },

    colunas_create: function () {
        return "id integer primary key, id_mesa integer,  filial integer,  nome text , status text, idGarcom integer, idPedidoAberto integer";
    },

    colunas_save: function () {
      return   "id_mesa, filial, nome, status, idGarcom, idPedidoAberto";
    },
    create: function (banco) {
        banco.transaction(function(tx) {
             tx.executeSql('CREATE TABLE IF NOT EXISTS '+db_mesas.nm_table()+' ('+db_mesas.colunas_create()+')');
        });
    },

    drop: function (banco) {
        banco.transaction(function(tx) {
            tx.executeSql('DROP TABLE '+db_mesas.nm_table());
        });
    },

    deleteById: function (banco, id) {
        banco.transaction(function(tx) {
            tx.executeSql('DELETE FROM '+db_mesas.nm_table()+" WHERE id_mesa ="+id);
        });
    },

    deleteAll: function (banco) {
        banco.transaction(function(tx) {
            tx.executeSql('DELETE FROM '+db_mesas.nm_table());
        });
    },

    save: function (banco, mesas_save) {

        db_mesas.deleteAll(banco);

        banco.transaction(function(tx) {
            jQuery.each(mesas_save, function(i, item) {
                dados = [item.id, item.filial, item.nome, item.status, item.idGarcom, item.idPedidoAberto];
                tx.executeSql("INSERT INTO "+db_mesas.nm_table()+" ("+db_mesas.colunas_save()+") VALUES (?, ? , ? , ? , ? , ?)", dados);
            });
        });
     },

    abrir_mesa: function (banco, mesa_id, pedido_id) {

        banco.transaction(function(tx) {
            dados = ['1', pedido_id];
            tx.executeSql("UPDATE "+db_mesas.nm_table()+" SET status = ? , idPedidoAberto = ? WHERE id_mesa = " +mesa_id, dados,
                function () {
                alert("a");
            }, function () {
                alert("b");
            });
        });
    },

    busca_byId: function (banco, id) {
        banco.transaction(function(tx) {
            // Faz uma busca na tabela
            tx.executeSql("SELECT * FROM "+db_mesas.nm_table()+" WHERE id_mesa = "+id+";", [], function(tx, res) {
                for (var i = 0;i<res.rows.length;i++){
                    alert("Linha "+i+": "+res.rows.item(i).titulo);
                }
            });
        });
    },

    buscarAll: function (banco) {
        jQuery("#listar_mesas").empty();

        jQuery.each(mesas_all, function(i, item) {
            var status = item.status;
            var nome = item.nome;
            var mesa_id = item.id;
            var innerA = '';

            if (status == 1) {
                innerA = '' +
                    '<li>' +
                    '   <a href="#" name="clickMesas">Mesa '+nome+
                    '       <img src="../www/img/mesafechada.png" width="70" height="57" style="margin-top: 15px;margin-left: 10px;" border="0">' +
                    '   </a>' +
                    '</li>';
            } else {
                innerA = '' +
                    '<li>' +
                    '   <a href="#" onclick="abrirMesa(this);" mesa_id="'+mesa_id+'" mesa_nome="'+nome+'">'+
                    '       <img src="../www/img/mesaaberta.png" width="70" height="57" style="margin-top: 8px;margin-left: 10px;" border="0">' +
                    '       Mesa '+nome+'<br/>Abrir' +
                    '   </a>' +
                    '</li>';
            }
            jQuery("#listar_mesas").append(innerA);
        });
        $('ul').listview().listview('refresh');
    },

    buscarAll2: function (banco) {
        jQuery("#listar_mesas").empty();

        banco.transaction(function(tx) {
            // Faz uma busca na tabela
            tx.executeSql("SELECT * FROM "+db_mesas.nm_table()+";", [], function(tx, res) {

                jQuery("#listar_mesas").empty();

                for (var i = 0;i<res.rows.length;i++){

                    var status = res.rows.item(i).status;
                    var nome = res.rows.item(i).nome;
                    var mesa_id = res.rows.item(i).id_mesa;
                    var innerA = '';

                    if (status == 1) {
                        innerA = '' +
                            '<li>' +
                            '   <a href="#" name="clickMesas">Mesa '+nome+
                            '       <img src="../www/img/mesafechada.png" width="70" height="57" style="margin-top: 15px;margin-left: 10px;" border="0">' +
                            '   </a>' +
                            '</li>';
                    } else {
                        innerA = '' +
                            '<li>' +
                            '   <a href="#" onclick="abrirMesa(this);" mesa_id="'+mesa_id+'" mesa_nome="'+nome+'">'+
                            '       <img src="../www/img/mesaaberta.png" width="70" height="57" style="margin-top: 8px;margin-left: 10px;" border="0">' +
                            '       Mesa '+nome+'<br/>Abrir' +
                            '   </a>' +
                            '</li>';
                    }
                    jQuery("#listar_mesas").append(innerA);
                }

                $('ul').listview().listview('refresh');
            });
        });
    },

    sincronizar: function (banco, base_url) {
        jQuery.ajax({
            url		: base_url + 'mesas/get_mesas_json/' ,
            type	: 'POST',
            cache	: false,
            async 	: false
        }).done(function(mesas){
            var mesasJon = JSON.parse(mesas);
            mesas_all = mesasJon
            db_mesas.save(banco, mesasJon);
        });

    }

};