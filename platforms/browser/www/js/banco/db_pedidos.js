
var db_pedidos = {

    nm_table: function () {
        return "pedidos";
    },

    colunas_create: function () {
        return " id integer primary key, " +
            "   pedido_id integer," +
            "   mesa_id integer, " +
            "   nome_mesa text ";
    },

    colunas_save: function () {
        return   "pedido_id, mesa_id, nome_mesa";
    },

    create: function (banco) {
        banco.transaction(function(tx) {
            tx.executeSql('CREATE TABLE IF NOT EXISTS '+db_pedidos.nm_table()+' ('+db_pedidos.colunas_create()+')');
        });
    },

    drop: function (banco) {
        banco.transaction(function(tx) {
            tx.executeSql('DROP TABLE '+db_pedidos.nm_table());
        });
    },

    deleteById: function (banco, id) {
        banco.transaction(function(tx) {
            tx.executeSql('DELETE FROM '+db_pedidos.nm_table()+" WHERE pedido_id ="+id);
        });
    },

    deleteAll: function (banco) {
        banco.transaction(function(tx) {
            tx.executeSql('DELETE FROM '+db_pedidos.nm_table());
        });
    },

    save: function (banco, pedido_save) {
        banco.transaction(function(tx) {
            tx.executeSql("INSERT INTO "+db_pedidos.nm_table()+" ("+db_pedidos.colunas_save()+") VALUES (? , ? , ? )", pedido_save,
                function () {
                    tx.executeSql("SELECT max(id) as maior_id , max(mesa_id) as mesa_id FROM "+db_pedidos.nm_table()+";", [], function(tx, res) {
                        for (var i = 0;i<res.rows.length;i++){
                            PEDIDO_ID = res.rows.item(i).maior_id;
                            mesa_id_mobile = res.rows.item(i).mesa_id;
                            dados = ['1', PEDIDO_ID, mesa_id_mobile];
                            tx.executeSql("UPDATE "+db_mesas.nm_table()+" SET status = ? , idPedidoAberto = ? WHERE id_mesa = ?", dados);
                        }
                    });
                }, function () {});
        });
    },

    enviar: function (banco) {
        banco.transaction(function(tx) {
            // Faz uma busca na tabela
            tx.executeSql("SELECT * FROM "+db_pedidos.nm_table()+" WHERE id = "+PEDIDO_ID+";", [], function(tx, res) {
                for (var i = 0;i<res.rows.length;i++){
                    mesa_id = res.rows.item(i).mesa_id;
                    jQuery.ajax({
                        url		: base_url + 'pdv/enviar_pedido_mobile/' ,
                        type	: 'POST',
                        data	: {
                            pedido_mobile_id : PEDIDO_ID,
                            mesa_id : mesa_id
                        },
                        cache	: false,
                        async 	: true
                    }).done(function(retorno){
                        dados = [retorno, PEDIDO_ID];
                        //tx.executeSql("UPDATE "+db_pedidos.nm_table()+" SET pedido_id = ? , idPedidoAberto = ? WHERE id = ?", dados);
                        alert('Impresso com sucesso na cozinha! '+retorno);
                    });
                }
            });
        });
    },

    sincronizar: function (banco, base_url) {
        jQuery.ajax({
            url		: base_url + 'produtos/get_produtos_json/' ,
            type	: 'POST',
            cache	: false,
            async 	: false
        }).done(function(produtos){
            var produtos_json = JSON.parse(produtos);
            produtos_all = produtos_json;
            db_produtos.save(banco, produtos_json);
        });

    }

};