var produtos_all = null;

var db_produtos = {

    nm_table: function () {
        return "produtos";
    },

    colunas_create: function () {
        return "id integer primary key, id_produto integer, categoria integer, valor  integer, filial integer,  nome text , img text ";
    },

    colunas_save: function () {
        return   "id_produto, filial, nome, categoria, valor, img";
    },
    create: function (banco) {
        banco.transaction(function(tx) {
            tx.executeSql('CREATE TABLE IF NOT EXISTS '+db_produtos.nm_table()+' ('+db_produtos.colunas_create()+')');
        });
    },

    drop: function (banco) {
        banco.transaction(function(tx) {
            tx.executeSql('DROP TABLE '+db_produtos.nm_table());
        });
    },

    deleteById: function (banco, id) {
        banco.transaction(function(tx) {
            tx.executeSql('DELETE FROM '+db_produtos.nm_table()+" WHERE id_produto ="+id);
        });
    },

    deleteAll: function (banco) {
        banco.transaction(function(tx) {
            tx.executeSql('DELETE FROM '+db_produtos.nm_table());
        });
    },

    save: function (banco, produtos_save) {

        db_produtos.deleteAll(banco);

        banco.transaction(function(tx) {
            jQuery.each(produtos_save, function(i, item) {
                dados = [item.id, item.filial, item.nome, item.categoria, item.valor, item.imagem ];
                tx.executeSql("INSERT INTO "+db_produtos.nm_table()+" ("+db_produtos.colunas_save()+") VALUES (?, ? , ? , ? , ? , ? )", dados);
            });
        });
    },

    busca_byId: function (banco, id) {
        banco.transaction(function(tx) {
            // Faz uma busca na tabela
            tx.executeSql("SELECT * FROM "+db_produtos.nm_table()+" WHERE id_produto = "+id+";", [], function(tx, res) {
                for (var i = 0;i<res.rows.length;i++){
                    alert("Linha "+i+": "+res.rows.item(i).titulo);
                }
            });
        });
    },

    buscarAll2: function (banco, categoria) {
        banco.transaction(function(tx) {
            // Faz uma busca na tabelaa
            tx.executeSql("SELECT * FROM "+db_produtos.nm_table()+" WHERE categoria = "+categoria+";", [], function(tx, res) {
                jQuery("#lista_produtos").empty();

                for (var i = 0;i<res.rows.length;i++){

                    var nome = res.rows.item(i).nome;

                    var  innerA = '' +
                        '<li>' +
                        '   <a href="#">'+nome+'</a>' +
                        '</li>';

                    jQuery("#lista_produtos").append(innerA);
                }
                $('ul').listview().listview('refresh');
            });
        });
    },

    buscarAll: function (banco, categoria) {
        jQuery("#lista_produtos").empty();
        jQuery.each(produtos_all, function(i, item) {
            if (item.categoria == categoria) {
                var nome = item.nome;
                var valor = item.valor;
                var produto_id = item.id;

                var innerA = '' +
                    '<li>' +
                    '   <a href="#" onclick="adicionarItemPedido(this)" produto_id="'+produto_id+'" valor_produto="'+valor+'" nome_produto="'+nome+'">' + nome + '</a>' +
                    '</li>';
                jQuery("#lista_produtos").append(innerA);
            }
        });
        $('ul').listview().listview('refresh');
    },

    sincronizar: function (banco, base_url) {
        jQuery.ajax({
            url		: base_url + 'produtos/get_produtos_json/' ,
            type	: 'POST',
            cache	: false,
            async 	: false
        }).done(function(produtos){
            var produtos_json = JSON.parse(produtos);
            produtos_all = produtos_json;
            db_produtos.save(banco, produtos_json);
        });

    }

};