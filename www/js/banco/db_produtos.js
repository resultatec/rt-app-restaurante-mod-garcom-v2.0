var produtos_all                = null;
var ADICIONAL_ID                = 12;
var QUANTIDADE_PRODUTOS         = 0;
var QUANTIDADE_PRODUTOS_ATUAL   = 0;

var db_produtos = {

    nm_table: function () {
        return "produtos";
    },

    colunas_create: function () {
        return " id integer primary key, " +
            "id_produto integer," +
            " categoria integer," +
            " valor  integer," +
            " filial integer, " +
            " nome text , " +
            " img text ," +
            " adicional integer , " +
            " usar_adicional integer  ";
    },

    colunas_save: function () {
        return   "" +
            " id_produto, " +
            " filial, " +
            " nome,  " +
            " categoria, " +
            " valor, " +
            " img, " +
            " adicional, " +
            " usar_adicional ";
    },
    create: function (banco) {
        banco.transaction(function(tx) {
            tx.executeSql('CREATE TABLE IF NOT EXISTS '+db_produtos.nm_table()+' ('+db_produtos.colunas_create()+')');
        });
    },

    drop: function (banco) {
        banco.transaction(function(tx) {
            tx.executeSql('DROP TABLE '+db_produtos.nm_table());
        });
    },

    deleteById: function (banco, id) {
        banco.transaction(function(tx) {
            tx.executeSql('DELETE FROM '+db_produtos.nm_table()+" WHERE id_produto ="+id);
        });
    },

    deleteAll: function (banco) {
        banco.transaction(function(tx) {
            tx.executeSql('DELETE FROM '+db_produtos.nm_table());
        });
    },

    save: function (banco, produtos_save) {

        db_produtos.deleteAll(banco);

        banco.transaction(function(tx) {
            jQuery.each(produtos_save, function(i, item) {
                produto_json = [
                    item.id,
                    item.filial,
                    item.nome,
                    item.categoria,
                    item.valor,
                    item.imagem ,
                    item.adicional,
                    item.usar_adicional
                ];
                tx.executeSql("INSERT INTO "+db_produtos.nm_table()+" ("+db_produtos.colunas_save()+") VALUES (?, ? , ? , ? , ? , ?, ? , ? )", produto_json);
            });
        });
    },

    busca_byId: function (banco, id) {
        banco.transaction(function(tx) {
            tx.executeSql("SELECT * FROM "+db_produtos.nm_table()+" WHERE id_produto = "+id+";", [], function(tx, res) {
                for (var i = 0;i<res.rows.length;i++){
                    alert("Linha "+i+": "+res.rows.item(i).titulo);
                }
            });
        });
    },

    buscarAllDbLocal: function (banco) {
        banco.transaction(function(tx) {
            tx.executeSql("SELECT * FROM "+db_produtos.nm_table()+";", [], function(tx, res) {

                for (var i = 0;i<res.rows.length;i++){

                    var nome        = res.rows.item(i).nome;
                    var valor       = res.rows.item(i).valor;
                    var id          = res.rows.item(i).id;
                    var id_produto  = res.rows.item(i).id_produto;
                    var categoria   = res.rows.item(i).categoria;
                    var img         = res.rows.item(i).img;
                    var adicional   = res.rows.item(i).adicional;
                    var usar_adicional = res.rows.item(i).usar_adicional;

                    var produtinho = new Object();

                    produtinho.id = id;
                    produtinho.id_produto = id_produto;
                    produtinho.nome = nome;
                    produtinho.valor = valor;
                    produtinho.categoria = categoria;
                    produtinho.img = img;
                    produtinho.adicional = adicional;
                    produtinho.usar_adicional = usar_adicional;

                    produtos_all[i] = produtinho;
                }

            });
        });
    },

    buscarAll: function (banco, categoria) {

        $("#lista_produtos").empty();
        $("#lista_produtos_consulta").empty();
        $("#listar_adicionais").empty();

        $.each(produtos_all, function(i, item) {

            var nome            = item.nome;
            var valor           = item.valor;
            var produto_id      = item.id;
            var usar_adicional  = item.usar_adicional;

            if (item.categoria == parseInt(categoria)) {

                var innerA = '' +
                    '<li>' +
                    '   <a href="#adicionarProduto" name="adicionarProdutoLink" usar_adicional="'+usar_adicional+'" produto_id="'+produto_id+'" valor_produto="'+valor+'" nome_produto="'+nome+'"><h2>' + nome + '</h2><strong><p>Preço (R$) '+valor+'</p></strong></a>' +
                    '</li>';

                var innerB = '' +
                    '<li>' +
                    '   <h2>' + nome + '</h2><strong><p>Preço (R$) '+valor+'</p></strong>' +
                    '</li>';

                $("#lista_produtos").append(innerA);
                $("#lista_produtos_consulta").append(innerB);

            } else if (item.categoria == ADICIONAL_ID) {

                var innerA = '';
                innerA = innerA + '<li data-icon="plus">';
                innerA = innerA + '    <a href="#" produto_id="'+produto_id+'" valor_produto="'+valor+'" nome_produto="'+nome+'"><h2>'+nome+'</h2><h2> R$ '+valor+' </h2>';
                innerA = innerA + '    <span class="ui-li-count">';
                innerA = innerA + '    <input type="number" style="background: #333333;width: 50px;text-align: center;color: #FFF;font-size: 15px;" id="quantidade_produto_id'+produto_id+'" value="0">';
                innerA = innerA + '   </span>';
                innerA = innerA + '    </a>';
                innerA = innerA + '    <a href="#" onclick="adicionarQuantidadeItem('+produto_id+')" data-rel="button">Adicionar mais itens adicionais</a>';
                innerA = innerA + '</li>';

                $("#listar_adicionais").append(innerA);
             }
        });
        $('ul').listview().listview('refresh');
    },
    buscarAdicionalEditar: function (banco, id_pedido_item, pedido_item_id) {

        $("#listar_adicionais_editar").empty();

        $.each(produtos_all, function(i, item) {

            var nome            = item.nome;
            var valor           = item.valor;
            var produto_id      = item.id_produto;

            if (produto_id === undefined) {
                produto_id = item.id;
            }

            if (item.categoria == ADICIONAL_ID) {

                var innerA = '';
                innerA = innerA + '<li data-icon="plus">';
                innerA = innerA + '    <a href="#" produto_id="'+produto_id+'" valor_produto="'+valor+'" nome_produto="'+nome+'"><h2>'+nome+'</h2><h2> R$ '+valor+' </h2>';
                innerA = innerA + '    <span class="ui-li-count">';
                innerA = innerA + '    <input type="number" onclick="editarQuantidadeItemEditar('+produto_id+')" style="background: #333333;width: 50px;text-align: center;color: #FFF;font-size: 15px;" id="quantidade_produto_id_editar'+produto_id+'" value="0">';
                innerA = innerA + '   </span>';
                innerA = innerA + '    </a>';
                innerA = innerA + '    <a href="#" onclick="adicionarQuantidadeItemEditar('+produto_id+')" data-rel="button">Adicionar mais itens adicionais</a>';
                innerA = innerA + '</li>';

                $("#listar_adicionais_editar").append(innerA);
            }
        });
        $('ul').listview().listview('refresh');
        db_pedido_item.setar_itens_adicional_pedido(id_pedido_item, pedido_item_id);
    },
    sincronizar: function (banco, base_url) {
        $.ajax({
            url		: base_url + 'produtos/get_produtos_json/' ,
            type	: 'POST',
            cache	: false,
            async 	: false
        }).done(function(produtos){
            var produtos_json = JSON.parse(produtos);
            produtos_all = produtos_json;
            db_produtos.save(banco, produtos_json);
        });

    },
    quantidade: function () {
        return QUANTIDADE_PRODUTOS;
    },

    setQuantidade: function (quantidade) {
        QUANTIDADE_PRODUTOS = quantidade;
    },

    quantidadeAtual: function () {
        return QUANTIDADE_PRODUTOS_ATUAL;
    },

    setQuantidadeAtual: function (quantidadeAtual) {
        QUANTIDADE_PRODUTOS_ATUAL = quantidadeAtual;
    }
};