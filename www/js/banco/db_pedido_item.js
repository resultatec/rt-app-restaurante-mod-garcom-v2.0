var itens_pedido_all = [];

var db_pedido_item = {

    nm_table: function () {
        return "pedido_item";
    },

    colunas_create: function () {
        return " id integer primary key, " +
            "   pedido_id integer," +
            "   pedido_mobile_id integer," +
            "   produto_id integer," +
            "   nome_produto text," +
            "   quantidade integer, " +
            "   valor integer, " +
            "   enviado integer, " +
            "   excluido integer, " +
            "   pedido_item_id integer, " +
            "   observacao text,  " +
            "   pedido_item_pai integer, " +
            "   quantidade_original integer, " +
            "   editado integer, " +
            "   pedido_item_pai_order integer";
    },

    colunas_save: function () {
        return   "pedido_id, " +
            " pedido_mobile_id, " +
            " produto_id," +
            " nome_produto, " +
            " quantidade, " +
            " valor, " +
            " observacao, " +
            " enviado , " +
            " excluido , " +
            " pedido_item_id, " +
            " pedido_item_pai, " +
            " quantidade_original," +
            " editado," +
            " pedido_item_pai_order ";
    },

    create: function (banco) {
        banco.transaction(function(tx) {
            tx.executeSql('CREATE TABLE IF NOT EXISTS '+db_pedido_item.nm_table()+' ('+db_pedido_item.colunas_create()+')');
        });
    },

    drop: function (banco) {
        banco.transaction(function(tx) {
            tx.executeSql('DROP TABLE '+db_pedido_item.nm_table());
        });
    },

    deleteById: function (banco, id) {
        banco.transaction(function(tx) {
            tx.executeSql('DELETE FROM '+db_pedido_item.nm_table()+" WHERE pedido_id ="+id);
        });
    },

    deleteAll: function (banco) {
        banco.transaction(function(tx) {
            tx.executeSql('DELETE FROM '+db_pedido_item.nm_table());
        });
    },

    save: function (banco, pedido_item_save) {
        banco.transaction(function(tx) {
            tx.executeSql("INSERT INTO "+db_pedido_item.nm_table()+" ("+db_pedido_item.colunas_save()+") VALUES (?, ? , ?, ?, ? , ? , ?, ? , ?, ?, ?, ? , ?, ?)", pedido_item_save,
                function () {
                    alert('Item adicionado com sucesso!');
                }, function () {
                    alert('Erro ao adicionar item 3');
                });
        });
    },

    editar: function (banco,pedido_item_save, id, pedido_item_id,  enviado,  adicionais_array) {

        var editado_item = 1;
        if(enviado === '0') {
            editado_item = 0;
        }
        banco.transaction(function(tx) {
            var sql = " UPDATE "+db_pedido_item.nm_table()+" SET quantidade = ? , observacao = ? , editado = "+editado_item+" WHERE id = "+id;
            tx.executeSql(sql,pedido_item_save,
                function () {
                    $.each(adicionais_array, function(i, item) {

                        var isAdicionado            = item.isAdicionado;
                        var enviado                 = 0;
                        var excluido                = 0;
                        var pedido_item_id_server   = 0
                        var observacao              = '';
                        var editado                 = 0;
                        var id_order                = id;

                        if (isAdicionado !== "1") {

                            if (parseInt(pedido_item_id) > 0) {
                                if (id !== pedido_item_id) {
                                    id = pedido_item_id + '_add';
                                    id_order = pedido_item_id;
                                }
                            }

                            var pedido_item_adicional_save = [
                                '',
                                item.pedido_id,
                                item.produto_id,
                                item.nome_produto,
                                item.quantidade,
                                item.valor_produto,
                                observacao,
                                enviado,
                                excluido,
                                pedido_item_id_server,
                                id,
                                item.quantidade,
                                editado,
                                id_order
                            ];

                            tx.executeSql("INSERT INTO " + db_pedido_item.nm_table() + " (" + db_pedido_item.colunas_save() + ") VALUES (?, ? , ?, ?, ? , ? , ?, ? , ? , ?, ?, ? , ?, ?)", pedido_item_adicional_save,
                                function () {
                                },
                                function () {
                                    alert('Erro ao adicionar item 1');
                                }
                            );
                        } else {

                            var editado = item.editado;

                            if (editado === "1") {

                                var itens_editar = [
                                    item.quantidade,
                                    item.idmobile
                                ];

                                var editado_item = 1;
                                if(enviado === '0') {
                                    editado_item = 0;
                                }

                                var sql = " UPDATE "+db_pedido_item.nm_table()+" SET quantidade = ? , editado = "+editado_item+" WHERE id = ? ";
                                tx.executeSql(sql,itens_editar,
                                    function () {
                                    }, function () {
                                        alert('error');
                                    }
                                );
                            }
                        }
                    });

                }, function () {
                    alert('Erro ao atualizar item');
                });
        });
    },

    save_silencioso: function (banco, pedido_item_save) {
        banco.transaction(function(tx) {
            tx.executeSql("INSERT INTO "+db_pedido_item.nm_table()+" ("+db_pedido_item.colunas_save()+") VALUES (?, ? , ?, ?, ? , ? , ?, ? , ? , ?, ?, ?, ?, ?)", pedido_item_save,
                function () {
                }, function () {
                    alert('Erro ao adicionar item 4');
                });
        });
    },

    save_silenciosoArray: function (banco, pedido_item_save, adicionais_array) {
        banco.transaction(function(tx) {
            tx.executeSql("INSERT INTO "+db_pedido_item.nm_table()+" ("+db_pedido_item.colunas_save()+") VALUES (?, ? , ?, ?, ? , ? , ?, ? , ? , ?, ?, ? , ? , ?)", pedido_item_save,
                function () {
                    tx.executeSql("SELECT max(id) as maior_id  FROM "+db_pedido_item.nm_table()+";", [], function(tx, res) {
                        for (var i = 0; i < res.rows.length; i++) {

                            PEDIDO_ITEM_ID_MAX = res.rows.item(i).maior_id;

                            $.each(adicionais_array, function(i, item) {

                                var enviado         = 0;
                                var excluido        = 0;
                                var pedido_item_id  = 0
                                var observacao      = '';
                                var editado         = 0;

                                var pedido_item_adicional_save = [
                                    '',
                                    item.pedido_id,
                                    item.produto_id,
                                    item.nome_produto ,
                                    item.quantidade,
                                    item.valor_produto,
                                    observacao,
                                    enviado,
                                    excluido ,
                                    pedido_item_id,
                                    PEDIDO_ITEM_ID_MAX,
                                    item.quantidade,
                                    editado,
                                    PEDIDO_ITEM_ID_MAX
                                ];

                                tx.executeSql("INSERT INTO "+db_pedido_item.nm_table()+" ("+db_pedido_item.colunas_save()+") VALUES (?, ? , ?, ?, ? , ? , ?, ? , ? , ?, ?, ? , ?, ?)", pedido_item_adicional_save,
                                    function () {},
                                    function () {
                                        alert('Erro ao adicionar item 1');
                                    }
                                );

                                var dados_item = [PEDIDO_ITEM_ID_MAX, PEDIDO_ITEM_ID_MAX];
                                tx.executeSql("UPDATE "+db_pedido_item.nm_table()+" SET pedido_item_pai_order = ?  WHERE id = ?", dados_item);
                            });
                        }
                    });
                }, function () {
                    alert('Erro ao adicionar item 2');
                });
        });
    },

    excluir: function (banco, id_pedido_item, pedido_item_id) {
        $.ajax({
            url		: base_url + 'pdv/excluir_item_aplicativo/' ,
            type	: 'POST',
            data	: {
                pedido_item_id: pedido_item_id,
            },
            cache	: false,
            async 	: true
        }).done(function(retorno){
            banco.transaction(function(tx) {
                var excluido = 1;

                dados = [
                    excluido,
                    id_pedido_item
                ];
                
                tx.executeSql("UPDATE "+db_pedido_item.nm_table()+" SET excluido = ?  WHERE id = ?", dados,
                    function () {},
                    function () {}
                    );


                if (parseInt(pedido_item_id) > 0) {

                    tx.executeSql("UPDATE "+db_pedido_item.nm_table()+" SET excluido = ?  WHERE pedido_item_pai = ?", dados,
                        function () {},
                        function () {}
                    );

                    dados2 = [excluido, pedido_item_id];
                    tx.executeSql("UPDATE " + db_pedido_item.nm_table() + " SET excluido = ?  WHERE pedido_item_pai = ?", dados2,
                        function () {
                            db_pedido_item.buscar_all_itens_pedido(banco);
                            $.mobile.changePage("#fechamento_pedido");
                        },
                        function () {
                        }
                    );
                } else {
                    tx.executeSql("UPDATE "+db_pedido_item.nm_table()+" SET excluido = ?  WHERE pedido_item_pai = ?", dados,
                        function () {
                            db_pedido_item.buscar_all_itens_pedido(banco);
                            $.mobile.changePage("#fechamento_pedido");
                        },
                        function () {}
                    );
                }
            });
        });
    },

    buscar_all_itens_pedido: function (banco) {
        banco.transaction(function(tx) {

            var table = $('#table_pedidos_itens');
            var $tbody = table.append('<tbody />').children('tbody');
            $tbody.empty();
            itens_pedido_all = [];

            tx.executeSql("SELECT * FROM "+db_pedido_item.nm_table()+" where excluido = 0 and pedido_mobile_id = "+PEDIDO_ID+ " ORDER BY pedido_item_pai_order , pedido_item_pai ", [], function(tx, res) {
                if (res.rows.length > 0) {
                    var table = $('#table_pedidos_itens');
                    var $tbody = table.append('<tbody />').children('tbody');
                    $tbody.empty();

                    var total = 0;

                    for (var i = 0; i < res.rows.length; i++) {

                        var id_pedido_item  = res.rows.item(i).id;
                        var nome_produto    = res.rows.item(i).nome_produto;
                        var quantidade      = res.rows.item(i).quantidade;
                        var valor           = res.rows.item(i).valor;
                        var observacao      = res.rows.item(i).observacao;
                        var enviado         = res.rows.item(i).enviado;
                        var pedido_item_id  = res.rows.item(i).pedido_item_id;
                        var pedido_item_pai = res.rows.item(i).pedido_item_pai;
                        var observacaoLimpo = res.rows.item(i).observacao;
                        var editado         = res.rows.item(i).editado;
                        var produto_id      = res.rows.item(i).produto_id;
                        var background      = '#d76a7a';

                        total = total + (quantidade*valor);

                        var itens = new Object();
                        itens.id            = id_pedido_item;
                        itens.nome_produto  = nome_produto;
                        itens.quantidade    = quantidade;
                        itens.valor         = valor;
                        itens.observacao    = observacao;
                        itens.enviado       = enviado;
                        itens.pedido_item_id = pedido_item_id;
                        itens.pedido_item_pai = pedido_item_pai;
                        itens.editado       = editado;
                        itens.produto_id    = produto_id;

                        itens_pedido_all[i] = itens;

                        if (observacao != '') {
                            observacao = ' ('+observacao+')';
                        }

                        if (isNaN(pedido_item_pai)) {
                            pedido_item_pai = pedido_item_pai.replace("_add", "");
                        }

                        if (enviado === 0) {
                            if (pedido_item_pai > 0 ) {
                                $tbody.append('<tr />').children('tr:last')
                                    .append("<td name='editar_item' colspan='4' enviado='"+enviado+"' pedido_item_pai='"+pedido_item_pai+"' observacao='"+observacaoLimpo+"' total='"+total+"' quantidade='"+quantidade+"' nome_produto='"+nome_produto+"' pedido_item_id='"+pedido_item_id+"' id_pedido_item='"+id_pedido_item+"' style='background-color: "+background+";text-align: left;cursor:pointer;'>" +
                                        "   <a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; |- "+nome_produto+" / Qtd: "+quantidade+" Total R$ "+ (quantidade*valor) +"</a>"+observacao+"" +
                                        "</td>");
                            } else {
                                $tbody.append('<tr />').children('tr:last')
                                    .append("<td name='editar_item' colspan='4' enviado='"+enviado+"' pedido_item_pai='"+pedido_item_pai+"' observacao='"+observacaoLimpo+"' total='"+total+"' quantidade='"+quantidade+"' nome_produto='"+nome_produto+"' pedido_item_id='"+pedido_item_id+"' id_pedido_item='"+id_pedido_item+"' style='background-color: "+background+";text-align: left;cursor:pointer;'>" +
                                        "<a>"+nome_produto+" / Qtd: "+quantidade+" Total R$"+(quantidade*valor)+"</a>"+observacao+
                                        "</td>");
                               }

                        } else {

                            if (editado === 1) {
                                background = '#059';
                            } else {
                                background = '';
                            }

                            if (pedido_item_pai > 0 ) {
                                $tbody.append('<tr />').children('tr:last')
                                    .append("<td name='editar_item'  colspan='4' enviado='"+enviado+"' pedido_item_pai='"+pedido_item_pai+"' observacao='"+observacaoLimpo+"' total='"+total+"' quantidade='"+quantidade+"' nome_produto='"+nome_produto+"' pedido_item_id='" + pedido_item_id + "' id_pedido_item='" + id_pedido_item + "' style='background-color: "+background+";text-align: left;cursor: pointer'>" +
                                        "<a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; |- " + nome_produto +" / Qtd: "+quantidade+" Total R$" + (quantidade * valor) + "</a>" + observacao +
                                        "</td>");
                            } else {
                                $tbody.append('<tr />').children('tr:last')
                                    .append("<td name='editar_item'  colspan='4'  enviado='"+enviado+"' pedido_item_pai='"+pedido_item_pai+"' observacao='"+observacaoLimpo+"' total='"+total+"' quantidade='"+quantidade+"' nome_produto='"+nome_produto+"' pedido_item_id='" + pedido_item_id + "' id_pedido_item='" + id_pedido_item + "' style='background-color:"+background+";text-align: left;cursor: pointer'>" +
                                        "<a>" + nome_produto + " / Qtd: "+quantidade+" Total R$" + (quantidade * valor) + "</a>" + observacao +
                                        "</td>");
                            }
                        }
                    }

                    $tbody.append('<tr />').children('tr:last')
                        .append("<td colspan='4' style='text-align: left;'><hr><b>SUB-TOTAL</b> R$ "+total+"</td>");

                    $.unblockUI();
                }
            });
        });
    },

    setar_itens_adicional_pedido: function (id_pedido_item, pedido_item_id) {

        $.each(itens_pedido_all, function(i, item) {

            var pedido_item_pai = item.pedido_item_pai;

            if (isNaN(pedido_item_pai)) {
                pedido_item_pai = pedido_item_pai.replace("_add", "");
                pedido_item_pai = parseInt(pedido_item_pai);
            }

            if (parseInt(id_pedido_item) === pedido_item_pai ||
                parseInt(pedido_item_id) === pedido_item_pai ) {

                var quantidade  = item.quantidade;
                var produto_id  = item.produto_id;
                var id          = item.id;

                var tagQuantidadeAdicional = $('#quantidade_produto_id_editar' + produto_id);

                if (tagQuantidadeAdicional !== undefined) {
                    tagQuantidadeAdicional.val(quantidade);
                    tagQuantidadeAdicional.attr("isAdicionado", 1);
                    tagQuantidadeAdicional.attr("idmobile", id);
                }
            }
        });
     },

    busca_byId: function (banco, id) {
        banco.transaction(function(tx) {
            tx.executeSql("SELECT * FROM "+db_pedidos.nm_table()+" WHERE pedido_id = "+id+";", [], function(tx, res) {
                for (var i = 0;i<res.rows.length;i++){
                    alert("Linha "+i+": "+res.rows.item(i).titulo);
                }
            });
        });
    },

    sincronizar: function (banco, base_url) {
        jQuery.ajax({
            url		: base_url + 'produtos/get_produtos_json/' ,
            type	: 'POST',
            cache	: false,
            async 	: false
        }).done(function(produtos){
            var produtos_json = JSON.parse(produtos);
            produtos_all = produtos_json;
            db_produtos.save(banco, produtos_json);
        });
    }

};