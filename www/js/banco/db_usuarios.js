var usuarios_all    = [];
var USUARIO         = null;
var NOME_USUARIO    = null;

var db_usuarios = {

    nm_table: function () {
        return "usuarios";
    },

    colunas_create: function () {
        return "id integer primary key, nome text, usuario text,  senha text , usuario_id integer ";
    },

    colunas_save: function () {
        return   "nome, usuario, senha, usuario_id";
    },

    create: function (banco) {
        banco.transaction(function(tx) {
            tx.executeSql('CREATE TABLE IF NOT EXISTS '+db_usuarios.nm_table()+' ('+db_usuarios.colunas_create()+')');
        });
    },

    drop: function (banco) {
        banco.transaction(function(tx) {
            tx.executeSql('DROP TABLE '+db_usuarios.nm_table());
        });
    },

    deleteAll: function (banco) {
        banco.transaction(function(tx) {
            tx.executeSql('DELETE FROM '+db_usuarios.nm_table());
        });
    },

    save: function (banco, usuarios_save) {

        db_usuarios.deleteAll(banco);

        banco.transaction(function(tx) {
            jQuery.each(usuarios_save, function(i, item) {
                dados = [item.nome, item.usuario, item.senha, item.usuario_id ];
                tx.executeSql("INSERT INTO "+db_usuarios.nm_table()+" ("+db_usuarios.colunas_save()+") VALUES (?, ? , ? , ? )", dados);
            });
        });
    },

    logar: function (banco,usuario, senha) {
        banco.transaction(function(tx) {
            tx.executeSql("SELECT * FROM " + db_usuarios.nm_table() + " where usuario ='"+usuario+"' and senha = '"+senha+"'", [], function (tx, res) {
                if (res.rows.length === 0) {
                    alert("Login / Senha Inválida!");
                } else {
                    for (var i = 0; i < res.rows.length; i++) {
                        USUARIO         = res.rows.item(i).usuario_id;
                        NOME_USUARIO    = res.rows.item(i).nome;
                        $.mobile.changePage("#mesas");
                    }
                }
            });
        });
    },

    buscarAllDbLocal: function (banco, base_url) {
        banco.transaction(function(tx) {
            tx.executeSql("SELECT * FROM "+db_usuarios.nm_table()+";", [], function(tx, res) {
                if (res.rows.length === 0) {
                    db_usuarios.sincronizar(banco, base_url);
                } else {

                    for (var i = 0; i < res.rows.length; i++) {

                        var id = res.rows.item(i).id;
                        var nome = res.rows.item(i).nome;
                        var usuario = res.rows.item(i).usuario;
                        var senha = res.rows.item(i).senha;
                        var usuario_id = res.rows.item(i).usuario_id;

                        var usuariosinho = new Object();

                        usuariosinho.id = id;
                        usuariosinho.nome = nome;
                        usuariosinho.usuario = usuario;
                        usuariosinho.senha = senha;
                        usuariosinho.usuario_id = usuario_id;
                        usuarios_all[i] = usuariosinho;
                    }

                    db_usuarios.popularUsuariosLogim();
                }
            });
        });
    },

    sincronizar: function (banco, base_url) {
        $.ajax({
            url		: base_url + 'usuarios/get_usuarios_json/' ,
            type	: 'POST',
            cache	: false,
            async 	: false
        }).done(function(usuarios){
            usuariosJson = JSON.parse(usuarios);
            usuarios_all = usuariosJson;
            db_usuarios.popularUsuariosLogim();
            db_usuarios.save(banco,usuariosJson);
        });

    },

    popularUsuariosLogim: function () {
        $("#imput_login").empty();
        $.each(usuarios_all, function(i, item) {
            $('#imput_login').append($('<option>', {
                value: item.usuario,
                text : item.nome
            }));
        });
    },

    getNomeUsuario: function () {
        return NOME_USUARIO;
    }

};