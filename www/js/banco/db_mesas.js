var mesas_all = null;

var db_mesas = {

    nm_table: function () {
        return "mesas";
    },

    colunas_create: function () {
        return "id integer primary key," +
            " id_mesa integer,  " +
            " filial integer," +
            " nome text , " +
            " status text, " +
            " idGarcom integer," +
            " importada integer," +
            " nome_garcom text, " +
            " idPedidoAberto integer, " +
            " idPedidoAbertoSistema integer ";
    },

    colunas_save: function () {
      return   "id_mesa, filial, nome, status, idGarcom, idPedidoAberto, importada, nome_garcom, idPedidoAbertoSistema";
    },

    create: function (banco) {
        banco.transaction(function(tx) {
             tx.executeSql('CREATE TABLE IF NOT EXISTS '+db_mesas.nm_table()+' ('+db_mesas.colunas_create()+')');
        });
    },

    drop: function (banco) {
        banco.transaction(function(tx) {
            tx.executeSql('DROP TABLE '+db_mesas.nm_table());
        });
    },

    deleteById: function (banco, id) {
        banco.transaction(function(tx) {
            tx.executeSql('DELETE FROM '+db_mesas.nm_table()+" WHERE id_mesa ="+id);
        });
    },

    deleteAll: function (banco) {
        banco.transaction(function(tx) {
            tx.executeSql('DELETE FROM '+db_mesas.nm_table());
        });
    },

    save: function (banco, mesas_save) {

        db_mesas.deleteAll(banco);

        banco.transaction(function(tx) {
            jQuery.each(mesas_save, function(i, item) {
                dados = [item.id, item.filial, item.nome, '0', '', '', 0, item.nome_garcom, item.idPedidoAbertoSistema];
                tx.executeSql("INSERT INTO "+db_mesas.nm_table()+" ("+db_mesas.colunas_save()+") VALUES (?, ? , ? , ? , ? , ? , ? , ? , ? )", dados);
            });
        });
    },

    busca_byId: function (banco, id) {
        banco.transaction(function(tx) {
            // Faz uma busca na tabela
            tx.executeSql("SELECT * FROM "+db_mesas.nm_table()+" WHERE id_mesa = "+id+";", [], function(tx, res) {
                for (var i = 0;i<res.rows.length;i++){
                    alert("Linha "+i+": "+res.rows.item(i).titulo);
                }
            });
        });
    },

    buscarAll: function () {
        jQuery("#listar_mesas").empty();

        jQuery.each(mesas_all, function(i, item) {

            var status              = item.status;
            var nome                = item.nome;
            var mesa_id             = item.id;
            var idPedidoAberto      = item.idPedidoAberto;
            var nome_garcom         = item.nome_garcom;
            var idPedidoAbertoSistema = item.idPedidoAbertoSistema;

            if (nome_garcom === undefined){
                nome_garcom = '';
            }

            var innerA = '';

            if ( parseInt(status) === 1) {
                innerA = '' +
                    '<li data-icon="plus" >' +
                    '   <a href="#" name="clickMesas" idPedidoAbertoSistema="'+idPedidoAbertoSistema+'" mesa_id="'+mesa_id+'" mesa_nome="'+nome+'" idPedidoAberto="'+idPedidoAberto+'" data-role="button">' +
                    '       <img src="../www/img/mesaaberta.png" width="70" height="57" style="margin-top: 15px;margin-left: 10px;" border="0">' +
                    '       Mesa '+nome+'<br/>'+nome_garcom+
                    '   </a>' +
                    '   <a href="#" name="clickMesasVerCategorias" mesa_id="'+mesa_id+'" mesa_nome="'+nome+'" idPedidoAberto="'+idPedidoAberto+'" data-role="button"> Adicionar item</a> ' +
                    '</li>';
            } else {
                innerA = '' +
                    '<li data-icon="plus" >' +
                    '   <a href="#" onclick="abrirMesa(this);" idPedidoAbertoSistema="'+idPedidoAbertoSistema+'" mesa_id="'+mesa_id+'" mesa_nome="'+nome+'" data-role="button">'+
                    '       <img src="../www/img/mesafechada.png" width="70" height="57" style="margin-top: 8px;margin-left: 10px;" border="0">' +
                    '       Mesa '+nome+'<br/>Abrir' +
                    '   </a>' +
                    '   <a href="#" onclick="abrirMesa(this);" mesa_id="'+mesa_id+'" mesa_nome="'+nome+'" data-role="button">Adicionar item </a>'+
                '</li>';
            }
            jQuery("#listar_mesas").append(innerA);
        });
        $('ul').listview().listview('refresh');
    },

    fechar_mesa_importada: function (banco) {

        $("#listar_mesas").empty();

        $.each(mesas_all, function(i, item) {

            var status = item.status;
            var nome = item.nome;
            var mesa_id = item.id;
            var idPedidoAberto = item.idPedidoAberto;
            var importada = item.importada;
            var nome_garcom = item.nome_garcom;
            var idPedidoAbertoSistema = item.idPedidoAbertoSistema;

            if (nome_garcom === undefined){
                nome_garcom = '';
            }

            var innerA = '';

            if ( parseInt(status) === 1) {

                if (importada === 1) {

                    mesas_all[i].status                 = 0;
                    mesas_all[i].idPedidoAberto         = null;
                    mesas_all[i].idPedidoAbertoSistema  = null;
                    mesas_all[i].importada              = 0;
                    mesas_all[i].nome_garcom            = '';

                    innerA = '' +
                        '<li data-icon="plus" >' +
                        '   <a href="#" onclick="abrirMesa(this);" idPedidoAbertoSistema="'+idPedidoAbertoSistema+'" mesa_id="'+mesa_id+'" mesa_nome="'+nome+'" data-role="button">'+
                        '       <img src="../www/img/mesafechada.png" width="70" height="57" style="margin-top: 8px;margin-left: 10px;" border="0">' +
                        '       Mesa '+nome+'<br/>Abrir' +
                        '   </a>' +
                        '   <a href="#" onclick="abrirMesa(this);" idPedidoAbertoSistema="'+idPedidoAbertoSistema+'" mesa_id="'+mesa_id+'" mesa_nome="'+nome+'" data-role="button">Adicionar item</a>'+
                        '</li>';

                    dados = ['0', null, 0,  idPedidoAberto];

                    banco.transaction(function(tx) {
                        tx.executeSql("UPDATE " + db_mesas.nm_table() + " SET status = ? , idPedidoAberto = ? , importada = ? WHERE idPedidoAberto = ?", dados);
                    });
                } else {
                    innerA = '' +
                        '<li data-icon="plus" >' +
                        '   <a href="#" name="clickMesas" idPedidoAbertoSistema="'+idPedidoAbertoSistema+'" mesa_id="'+mesa_id+'" mesa_nome="' + nome + '" idPedidoAberto="' + idPedidoAberto + '" data-role="button">' +
                        '       <img src="../www/img/mesaaberta.png" width="70" height="57" style="margin-top: 15px;margin-left: 10px;" border="0">' +
                        '       Mesa '+nome+'<br/>'+nome_garcom+
                        '   </a>' +
                        '   <a href="#" name="clickMesasVerCategorias" mesa_nome="' + nome + '" idPedidoAberto="' + idPedidoAberto + '" data-role="button">Adicionar item</a>' +
                         '</li>';
                }
            } else {
                innerA = '' +
                    '<li data-icon="plus" >' +
                    '   <a href="#" onclick="abrirMesa(this);" idPedidoAbertoSistema="'+idPedidoAbertoSistema+'" mesa_id="'+mesa_id+'" mesa_nome="'+nome+'" data-role="button">'+
                    '       <img src="../www/img/mesafechada.png" width="70" height="57" style="margin-top: 8px;margin-left: 10px;" border="0">' +
                    '       Mesa '+nome+'<br/>Abrir' +
                    '   </a>' +
                    '   <a href="#" onclick="abrirMesa(this);" mesa_id="'+mesa_id+'" mesa_nome="'+nome+'" data-role="button">Adicionar item</a>'+
                    '</li>';
            }
            jQuery("#listar_mesas").append(innerA);
        });
        $('ul').listview().listview('refresh');
    },

    buscarAllDbLocal: function (banco) {
        $("#listar_mesas").empty();

        banco.transaction(function(tx) {
            // Faz uma busca na tabela
            tx.executeSql("SELECT * FROM "+db_mesas.nm_table()+";", [], function(tx, res) {

                $("#listar_mesas").empty();

                for (var i = 0;i<res.rows.length;i++){

                    var id = res.rows.item(i).id;
                    var status = res.rows.item(i).status;
                    var nome = res.rows.item(i).nome;
                    var mesa_id = res.rows.item(i).id_mesa;
                    var filial = res.rows.item(i).filial;
                    var idPedidoAberto = res.rows.item(i).idPedidoAberto;
                    var importada = res.rows.item(i).importada;
                    var lugares = 0;
                    var nome_garcom = res.rows.item(i).nome_garcom;
                    var idPedidoAbertoSistema = res.rows.item(i).idPedidoAbertoSistema;

                    if (nome_garcom === undefined){
                        nome_garcom = '';
                    }

                    var innerA = '';

                    var mesinha = new Object();
                    mesinha.id = id;
                    mesinha.filial = filial;
                    mesinha.nome = nome;
                    mesinha.lugares = lugares;
                    mesinha.id_mesa = mesa_id;
                    mesinha.status = status;
                    mesinha.idPedidoAberto = idPedidoAberto;
                    mesinha.importada = importada;
                    mesinha.nome_garcom = nome_garcom;
                    mesinha.idPedidoAbertoSistema = idPedidoAbertoSistema;

                    mesas_all[i] = mesinha;

                    if (parseInt(status) === 1) {
                        innerA = '' +
                            '<li data-icon="plus" >' +
                            '   <a href="#" name="clickMesas" idPedidoAbertoSistema="'+idPedidoAbertoSistema+'" mesa_id="'+mesa_id+'" mesa_nome="'+nome+'" idPedidoAberto="'+idPedidoAberto+'" data-role="button">' +
                            '       <img src="../www/img/mesaaberta.png" width="70" height="57" style="margin-top: 15px;margin-left: 10px;" border="0">' +
                            '       Mesa '+nome+'<br/>'+nome_garcom+
                            '   </a>' +
                            '   <a href="#" name="clickMesasVerCategorias" mesa_id="'+mesa_id+'" mesa_nome="'+nome+'" idPedidoAberto="'+idPedidoAberto+'" data-role="button">Adicionar item</a>' +
                            '</li>';
                    } else {
                        innerA = '' +
                            '<li data-icon="plus" >' +
                            '   <a href="#" onclick="abrirMesa(this);" idPedidoAbertoSistema="'+idPedidoAbertoSistema+'" mesa_id="'+mesa_id+'" mesa_nome="'+nome+'">'+
                            '       <img src="../www/img/mesafechada.png" width="70" height="57" style="margin-top: 8px;margin-left: 10px;" border="0">' +
                            '       Mesa '+nome+'<br/>Abrir' +
                            '   </a>' +
                            '   <a href="#" onclick="abrirMesa(this);" mesa_id="'+mesa_id+'" mesa_nome="'+nome+'">Adicionar item</a>'+
                            '</li>';
                    }
                    $("#listar_mesas").append(innerA);
                }

                $('ul').listview().listview('refresh');
            });
        });
    },

    sincronizar: function (banco, base_url) {
        $.ajax({
            url		: base_url + 'mesas/get_mesas_json/' ,
            type	: 'POST',
            cache	: false,
            async 	: false
        }).done(function(mesas){
            var mesasJon = JSON.parse(mesas);
            mesas_all = mesasJon
            db_mesas.save(banco, mesasJon);
        });
    },

    sincronizar_alfa: function (banco, base_url) {
        $.ajax({
            url		: base_url + 'mesas/get_mesas_json_alfa/' ,
            type	: 'POST',
            cache	: true,
            async 	: true
        }).done(function(mesas){
            var mesasJon = JSON.parse(mesas);
            mesas_all = mesasJon
            db_mesas.save(banco, mesasJon);
            db_mesas.buscarAll();
        });
    },

    popularMesasAbertaTransferencia: function () {
        $("#mesa_transferir").empty();
        $('#mesa_transferir').append($('<option>', {
            value: '',
            text: 'Selecione uma mesa'
        }));

        $('#mesa_transferir').val('').change();

        $.each(mesas_all, function(i, item) {
            var idPedidoAberto = item.idPedidoAberto;
            if (idPedidoAberto === 0 ||
                idPedidoAberto === '' ||
                idPedidoAberto === undefined) {

                $('#mesa_transferir').append($('<option>', {
                    value: item.id_mesa,
                    text: 'Mesa ' + item.nome
                }));
            }
        });
        $.mobile.changePage("#transferencia");
    },

    transferencia: function (banco, pedido_sistema_id, pedido_id, mesa_id, nome_mesa_atual, mesa_antiga_id) {

        if (mesa_id === ''){
            alert("Selecione uma mesa para realizar a transferência");
            return;
        }

        $.ajax({
            url		: base_url + 'pdv/transferir_mesa_mobileNew/' ,
            type	: 'POST',
            data	: {
                pedido_id   : pedido_sistema_id,
                mesa_id     : mesa_id
            },
            cache	: true,
            async 	: true
        }).done(function(mesa_antiga){

            banco.transaction(function(tx) {

                var nome_garcom     = db_usuarios.getNomeUsuario();
                var importada       = 1;
                var status_ocupada  = 1;

                if (mesa_antiga === '') {
                    importada = 0;
                }
                dados = [
                    status_ocupada,
                    nome_garcom,
                    importada ,
                    pedido_id,
                    pedido_sistema_id,
                    mesa_id
                ];
                tx.executeSql("UPDATE "+db_mesas.nm_table()+" SET status = ? , nome_garcom = ? , importada = ? , idPedidoAberto = ? , idPedidoAbertoSistema = ? WHERE id_mesa = ?", dados,
                    function () {},
                    function () {}
                );
            });

            banco.transaction(function (tx) {
                var status_ocupada  = 0;
                var nome_garcom     = '';
                var importada       = 0;
                dados = [
                    status_ocupada,
                    nome_garcom,
                    importada,
                    '',
                    '',
                    mesa_antiga_id
                ];
                tx.executeSql("UPDATE " + db_mesas.nm_table() + " SET status = ? , nome_garcom = ? , importada = ? , idPedidoAberto = ?, idPedidoAbertoSistema = ? WHERE id_mesa = ?", dados,
                    function () {
                    },
                    function () {}
                );
            });
            
            banco.transaction(function(tx) {
                var importada       = 1;
                var sql             = "UPDATE "+db_pedidos.nm_table()+" SET mesa_id = ? , importado = ? , nome_mesa = ? WHERE pedido_id = ?";
                if (mesa_antiga === '') {
                    importada = 0;
                    sql =   "UPDATE "+db_pedidos.nm_table()+" SET mesa_id = ? , importado = ? , nome_mesa = ?  WHERE id = ?"
                }

                dados = [
                    mesa_id,
                    importada ,
                    nome_mesa_atual,
                    pedido_id
                ];
                tx.executeSql(sql, dados,
                    function () {
                        db_mesas.buscarAllDbLocal(banco);
                        PEDIDO_ID = null;
                        $.mobile.changePage("#mesas");
                    },
                    function () {}
                );
            });

        });
    }

};