var categorias_all  = null;
var ADICIONAIS      = "ADICIONAIS";
var ENTREGA         = "ENTREGA";

var db_categorias = {

    nm_table: function () {
        return "categorias";
    },

    colunas_create: function () {
        return "id integer primary key, id_categoria integer, filial integer,  nome text , img text ";
    },

    colunas_save: function () {
        return   "id_categoria, filial, nome, img";
    },
    create: function (banco) {
        banco.transaction(function(tx) {
            tx.executeSql('CREATE TABLE IF NOT EXISTS '+db_categorias.nm_table()+' ('+db_categorias.colunas_create()+')');
        });
    },

    drop: function (banco) {
        banco.transaction(function(tx) {
            tx.executeSql('DROP TABLE '+db_categorias.nm_table());
        });
    },

    deleteById: function (banco, id) {
        banco.transaction(function(tx) {
            tx.executeSql('DELETE FROM '+db_categorias.nm_table()+" WHERE id_categoria ="+id);
        });
    },

    deleteAll: function (banco) {
        banco.transaction(function(tx) {
            tx.executeSql('DELETE FROM '+db_categorias.nm_table());
        });
    },

    save: function (banco, categorias_save) {

        db_categorias.deleteAll(banco);

        banco.transaction(function(tx) {
            jQuery.each(categorias_save, function(i, item) {
                dados = [item.id, item.filial, item.nome, item.img ];
                tx.executeSql("INSERT INTO "+db_categorias.nm_table()+" ("+db_categorias.colunas_save()+") VALUES (?, ? , ? , ? )", dados);
            });
        });
    },

    busca_byId: function (banco, id) {
        banco.transaction(function(tx) {
            // Faz uma busca na tabela
            tx.executeSql("SELECT * FROM "+db_categorias.nm_table()+" WHERE id_categoria = "+id+";", [], function(tx, res) {
                for (var i = 0;i<res.rows.length;i++){
                    alert("Linha "+i+": "+res.rows.item(i).titulo);
                }
            });
        });
    },

    buscarAll: function (banco) {
        $("#listar_categorias").empty();
        $('#listar_categorias_consulta').empty();

        jQuery.each(categorias_all, function(i, item) {
            var nome = item.nome;
            var id_categoria = item.id;

            if (nome != ADICIONAIS && nome != ENTREGA) {
                var innerA = '' +
                    '<li>' +
                    '   <a href="#produtos" name="clickCategorias" categoria="' + id_categoria + '">' + nome + '<br/>&nbsp;</a>' +
                    '</li>';

                $("#listar_categorias").append(innerA);
            }

            var innerB = '' +
                '<li>' +
                '   <a href="#produtos_consulta" name="clickCategorias" categoria="' + id_categoria + '">' + nome + '<br/>&nbsp;</a>' +
                '</li>';

            $('#listar_categorias_consulta').append(innerB);
        });
        $('ul').listview().listview('refresh');
    },

    buscarAllDbLocal: function (banco) {

        banco.transaction(function(tx) {
            // Faz uma busca na tabelaa
            tx.executeSql("SELECT * FROM "+db_categorias.nm_table()+";", [], function(tx, res) {

                $("#listar_categorias").empty();
                $('#listar_categorias_consulta').empty();

                for (var i = 0;i<res.rows.length;i++){

                    var nome = res.rows.item(i).nome;
                    var id_categoria = res.rows.item(i).id_categoria;
                    var filial = res.rows.item(i).filial;

                    var categoriasinha = new Object();
                    categoriasinha.id = id_categoria;
                    categoriasinha.filial = filial;
                    categoriasinha.nome = nome;

                    categorias_all[i] = categoriasinha;

                    if (nome != ADICIONAIS && nome !=  ENTREGA) {
                        var innerA = '' +
                            '<li>' +
                            '   <a href="#produtos" name="clickCategorias" categoria="' + id_categoria + '">' + nome + '<br/>&nbsp;</a>' +
                            '</li>';

                        $("#listar_categorias").append(innerA);
                    }

                    var innerB = '' +
                        '<li>' +
                        '   <a href="#produtos_consulta" name="clickCategorias" categoria="' + id_categoria + '">' + nome + '<br/>&nbsp;</a>' +
                        '</li>';
                    $('#listar_categorias_consulta').append(innerB);

                }
                $('ul').listview().listview('refresh');
            });
        });
    },
    sincronizar: function (banco, base_url) {
        jQuery.ajax({
            url		: base_url + 'categorias/get_categorias_json/' ,
            type	: 'POST',
            cache	: false,
            async 	: false
        }).done(function(categorias){
            categoriasJon = JSON.parse(categorias);
            categorias_all = categoriasJon;
            db_categorias.save(banco,categoriasJon);
        });

    }

};