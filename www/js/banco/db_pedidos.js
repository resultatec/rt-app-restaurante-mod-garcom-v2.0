
var pedidos_all_sincronizacao = null;
var pedidos_itens_all_sincronizacao = null;

var BANCO_ENGINER = null;
var PEDIDO_SINCRONIZAR = null;
var PEDIDO_ID_SINCRONIZAR = null;

var db_pedidos = {

    nm_table: function () {
        return "pedidos";
    },

    colunas_create: function () {
        return " id integer primary key, " +
            "   pedido_id integer," +
            "   mesa_id integer, " +
            "   status integer, " +
            "   importado integer, " +
            "   nome_mesa text ";
    },

    colunas_save: function () {
        return   "pedido_id, mesa_id, nome_mesa, status, importado";
    },

    create: function (banco) {
        banco.transaction(function(tx) {
            tx.executeSql('CREATE TABLE IF NOT EXISTS '+db_pedidos.nm_table()+' ('+db_pedidos.colunas_create()+')');
        });
    },

    drop: function (banco) {
        banco.transaction(function(tx) {
            tx.executeSql('DROP TABLE '+db_pedidos.nm_table());
        });
    },

    deleteAllItensImportadosByPedidoMobile: function (banco, id_pedido_mobile, id_pedido) {
        banco.transaction(function(tx) {
            tx.executeSql('DELETE FROM '+db_pedido_item.nm_table()+" WHERE enviado = 1 and pedido_mobile_id ="+id_pedido_mobile, [],
                function () {
                db_pedidos.save_sincronizar_item(BANCO_ENGINER, id_pedido);
            }, function () {
            })
        });
    },

    deleteAllItensImportadosByPedidoMobileViewPedido: function (banco, id_pedido_mobile, id_pedido) {
        banco.transaction(function(tx) {
            tx.executeSql('DELETE FROM '+db_pedido_item.nm_table()+" WHERE enviado = 1 and pedido_mobile_id ="+id_pedido_mobile, [],
                function () {
                    db_pedidos.save_sincronizar_item_pedido(BANCO_ENGINER, id_pedido);
                }, function () {
                })
        });
    },

    deleteAll: function (banco) {
        banco.transaction(function(tx) {
            tx.executeSql('DELETE FROM '+db_pedidos.nm_table());
        });
    },

    save: function (banco, pedido_save) {
        banco.transaction(function(tx) {
            tx.executeSql("INSERT INTO "+db_pedidos.nm_table()+" ("+db_pedidos.colunas_save()+") VALUES (? , ? , ? , ? , ?  )", pedido_save,
                function () {
                    tx.executeSql("SELECT max(id) as maior_id  FROM "+db_pedidos.nm_table()+";", [], function(tx, res) {
                        for (var i = 0;i<res.rows.length;i++){

                            PEDIDO_ID = res.rows.item(i).maior_id;

                            tx.executeSql("SELECT * FROM "+db_pedidos.nm_table()+" WHERE id = "+PEDIDO_ID, [], function(tx, res) {

                                for (var i = 0;i<res.rows.length;i++) {

                                    mesa_id_mobile = res.rows.item(i).mesa_id;
                                    dados = ['1', PEDIDO_ID, NOME_USUARIO, mesa_id_mobile];

                                    $.each(mesas_all, function (i, item) {
                                        if (item.id_mesa === parseInt(mesa_id_mobile) || item.id_mesa === ''+mesa_id_mobile) {
                                            mesas_all[i].status = 1;
                                            mesas_all[i].idPedidoAberto = PEDIDO_ID;
                                            mesas_all[i].nome_garcom = NOME_USUARIO;
                                            db_mesas.buscarAll(banco);
                                        }
                                    });
                                    tx.executeSql("UPDATE " + db_mesas.nm_table() + " SET status = ? , idPedidoAberto = ? , nome_garcom = ? WHERE id_mesa = ?", dados);
                                }
                            });

                        }
                    });
                }, function () {});
        });
    },

    save_sincronizar: function (banco, pedido_save) {
        banco.transaction(function(tx) {
            tx.executeSql("INSERT INTO "+db_pedidos.nm_table()+" ("+db_pedidos.colunas_save()+") VALUES (? , ? , ? , ? , ? )", pedido_save,
                function () {
                    tx.executeSql("SELECT max(id) as maior_id , pedido_id  FROM "+db_pedidos.nm_table()+";", [], function(tx, res) {
                        for (var i = 0;i<res.rows.length;i++){

                            PEDIDO_SINCRONIZAR      = res.rows.item(i).maior_id;
                            PEDIDO_ID_SINCRONIZAR   = res.rows.item(i).pedido_id;

                            tx.executeSql("SELECT * FROM "+db_pedidos.nm_table()+" WHERE id = "+PEDIDO_SINCRONIZAR, [], function(tx, res) {

                                for (var i = 0;i<res.rows.length;i++) {

                                    mesa_id_mobile  = res.rows.item(i).mesa_id;
                                    dados           = ['1', PEDIDO_SINCRONIZAR, 1, mesa_id_mobile ];

                                    //adicionando itens ao pedido
                                    $.each(pedidos_itens_all_sincronizacao, function(i, item) {

                                        if (PEDIDO_ID_SINCRONIZAR === parseInt(item.id_pedido) ) {

                                            var id_pedido           = item.id_pedido;
                                            var nome_produto        = item.nome_produto;
                                            var valor_produto       = item.valor;
                                            var id_produto          = item.id_produto;
                                            var observacao          = item.observacao;
                                            var quantidade          = item.quantidade;
                                            var pedido_item_id      = item.id;
                                            var nome_garcom         = item.nome_garcom
                                            var idItensPedidoOrigem   = item.idItensPedidoOrigem;
                                            var idItensPedidoOrigemOrder    = item.idItensPedidoOrigem;;

                                            if (parseInt(idItensPedidoOrigemOrder) === 0) {
                                                idItensPedidoOrigemOrder = pedido_item_id.replace("_add", "");
                                            }

                                            var pedido_item_save =
                                                [
                                                    id_pedido,
                                                    PEDIDO_SINCRONIZAR,
                                                    id_produto,
                                                    nome_produto,
                                                    quantidade,
                                                    valor_produto,
                                                    observacao,
                                                    1,
                                                    0,
                                                    pedido_item_id,
                                                    idItensPedidoOrigem,
                                                    quantidade,
                                                    0,
                                                    idItensPedidoOrigemOrder
                                            ];

                                            db_pedido_item.save_silencioso(BANCO_ENGINER, pedido_item_save);

                                            $.each(mesas_all, function (i, item) {
                                                if (item.id_mesa === parseInt(mesa_id_mobile) ||
                                                    item.id_mesa === '' + mesa_id_mobile) {

                                                    mesas_all[i].status                 = 1;
                                                    mesas_all[i].idPedidoAberto         = PEDIDO_SINCRONIZAR;
                                                    mesas_all[i].idPedidoAbertoSistema  = PEDIDO_ID_SINCRONIZAR;
                                                    mesas_all[i].importada              = 1;
                                                    mesas_all[i].nome_garcom            = nome_garcom;
                                                    db_mesas.buscarAll(banco);
                                                }
                                            });
                                        }
                                    });
                                    tx.executeSql("UPDATE " + db_mesas.nm_table() + " SET status = ? , idPedidoAberto = ? , importada = ? WHERE id_mesa = ?", dados);
                                }
                            });
                        }
                    });
                }, function () {});
        });
    },

    save_sincronizar_item: function (banco, pedido_id) {
        banco.transaction(function(tx) {
            tx.executeSql("SELECT id , pedido_id , mesa_id FROM "+db_pedidos.nm_table()+" WHERE pedido_id ="+pedido_id, [], function(tx, res) {

                for (var i = 0;i<res.rows.length;i++){

                    PEDIDO_SINCRONIZAR      = res.rows.item(i).id;
                    PEDIDO_ID_SINCRONIZAR   = res.rows.item(i).pedido_id;

                    mesa_id_mobile  = res.rows.item(i).mesa_id;
                    dados           =
                        [
                            '1',
                            PEDIDO_SINCRONIZAR,
                            1,
                            mesa_id_mobile
                        ];

                    //adicionando itens ao pedido
                    $.each(pedidos_itens_all_sincronizacao, function(i, item) {

                        if (PEDIDO_ID_SINCRONIZAR === parseInt(item.id_pedido) ) {

                            var id_pedido = item.id_pedido;
                            var nome_produto = item.nome_produto;
                            var valor_produto = item.valor;
                            var id_produto = item.id_produto;
                            var observacao = item.observacao;
                            var quantidade = item.quantidade;
                            var pedido_item_id = item.id;
                            var nome_garcom = item.nome_garcom;
                            var idItensPedidoOrigem         = item.idItensPedidoOrigem;
                            var idItensPedidoOrigemOrder    = item.idItensPedidoOrigem;;

                            if (parseInt(idItensPedidoOrigemOrder) === 0) {
                                idItensPedidoOrigemOrder = pedido_item_id.replace("_add", "");
                            }

                            var pedido_item_save =
                                [
                                    id_pedido,
                                    PEDIDO_SINCRONIZAR,
                                    id_produto,
                                    nome_produto,
                                    quantidade,
                                    valor_produto,
                                    observacao,
                                    1,
                                    0,
                                    pedido_item_id,
                                    idItensPedidoOrigem,
                                    quantidade,
                                    0,
                                    idItensPedidoOrigemOrder
                                ];

                            db_pedido_item.save_silencioso(BANCO_ENGINER, pedido_item_save);

                            $.each(mesas_all, function (i, item) {
                                if (item.id_mesa === parseInt(mesa_id_mobile) ||
                                    item.id_mesa === '' + mesa_id_mobile) {
                                    mesas_all[i].status                 = 1;
                                    mesas_all[i].idPedidoAberto         = PEDIDO_SINCRONIZAR;
                                    mesas_all[i].idPedidoAbertoSistema  = PEDIDO_ID_SINCRONIZAR;
                                    mesas_all[i].importada              = 1;
                                    mesas_all[i].nome_garcom            = nome_garcom;

                                    db_mesas.buscarAll(banco);
                                }
                            });
                        }
                    });
                    tx.executeSql("UPDATE " + db_mesas.nm_table() + " SET status = ? , idPedidoAberto = ? , importada = ? WHERE id_mesa = ?", dados,
                        function () {
                        },
                        function () {}
                        );
                }
            });
        });
    },

    save_sincronizar_item_pedido: function (banco, pedido_id) {
        banco.transaction(function(tx) {
            tx.executeSql("SELECT id , pedido_id , mesa_id FROM "+db_pedidos.nm_table()+" WHERE pedido_id ="+pedido_id, [], function(tx, res) {

                for (var i = 0;i<res.rows.length;i++){

                    PEDIDO_SINCRONIZAR      = res.rows.item(i).id;
                    PEDIDO_ID_SINCRONIZAR   = res.rows.item(i).pedido_id;

                    mesa_id_mobile  = res.rows.item(i).mesa_id;
                    dados           =
                        [
                            '1',
                            PEDIDO_SINCRONIZAR,
                            1,
                            mesa_id_mobile
                        ];

                    //adicionando itens ao pedido
                    $.each(pedidos_itens_all_sincronizacao, function(i, item) {

                        if (PEDIDO_ID_SINCRONIZAR === parseInt(item.id_pedido) ) {

                            var id_pedido       = item.id_pedido;
                            var nome_produto    = item.nome_produto;
                            var valor_produto   = item.valor;
                            var id_produto      = item.id_produto;
                            var observacao      = item.observacao;
                            var quantidade      = item.quantidade;
                            var pedido_item_id  = item.id;
                            var nome_garcom     = item.nome_garcom;
                            var idItensPedidoOrigem         = item.idItensPedidoOrigem;
                            var idItensPedidoOrigemOrder    = item.idItensPedidoOrigem;;

                            if (parseInt(idItensPedidoOrigemOrder) === 0) {
                                idItensPedidoOrigemOrder = pedido_item_id.replace("_add", "");
                            }

                            var pedido_item_save =
                                [
                                    id_pedido,
                                    PEDIDO_SINCRONIZAR,
                                    id_produto,
                                    nome_produto,
                                    quantidade,
                                    valor_produto,
                                    observacao,
                                    1,
                                    0,
                                    pedido_item_id,
                                    idItensPedidoOrigem,
                                    quantidade,
                                    0,
                                    idItensPedidoOrigemOrder
                                ];

                            db_pedido_item.save_silencioso(BANCO_ENGINER, pedido_item_save);

                            $.each(mesas_all, function (i, item) {
                                if (item.id_mesa === parseInt(mesa_id_mobile) ||
                                    item.id_mesa === '' + mesa_id_mobile) {
                                    mesas_all[i].status                 = 1;
                                    mesas_all[i].idPedidoAberto         = PEDIDO_SINCRONIZAR;
                                    mesas_all[i].idPedidoAbertoSistema  = PEDIDO_ID_SINCRONIZAR;
                                    mesas_all[i].importada              = 1;
                                    mesas_all[i].nome_garcom            = nome_garcom;
                                    db_mesas.buscarAll(banco);
                                }
                            });
                        }
                    });
                    tx.executeSql("UPDATE " + db_mesas.nm_table() + " SET status = ? , idPedidoAberto = ? , importada = ? WHERE id_mesa = ?", dados,
                        function () {
                            db_pedido_item.buscar_all_itens_pedido(banco);
                            $.mobile.changePage("#fechamento_pedido");
                        },
                        function () {}
                        );
                }
            });
        });
    },

    enviar: function (banco) {
        banco.transaction(function(tx) {
            tx.executeSql("SELECT * FROM "+db_pedidos.nm_table()+" WHERE id = "+PEDIDO_ID+";", [], function(tx, res) {

                var mesa_id = null;
                var pedido_id_gravado = null;

                for (var i = 0;i<res.rows.length;i++){
                    mesa_id = res.rows.item(i).mesa_id;
                    pedido_id_gravado = res.rows.item(i).pedido_id;
                }

                //novo produto
                tx.executeSql("SELECT  *" +
                    " FROM  pedido_item" +
                    " where excluido = 0 and " +
                    "       pedido_mobile_id = "+PEDIDO_ID, [], function(tx, res) {

                    var itens = [];

                    for (var i = 0;i<res.rows.length;i++){

                        var produto_id      = res.rows.item(i).produto_id;
                        var nome_produto    = res.rows.item(i).nome_produto;
                        var quantidade      = res.rows.item(i).quantidade;
                        var valor           = res.rows.item(i).valor;
                        var observacao      = res.rows.item(i).observacao;
                        var pedido_item_pai = res.rows.item(i).pedido_item_pai;
                        var pedido_item_id      = res.rows.item(i).pedido_item_id;
                        var enviado         = res.rows.item(i).enviado;
                        var editado         = res.rows.item(i).editado;

                        var item = [
                            produto_id,
                            nome_produto,
                            quantidade ,
                            valor ,
                            observacao,
                            pedido_item_pai,
                            enviado,
                            editado,
                            pedido_item_id
                        ];
                        itens[i] = item;
                    }

                    $.blockUI({ message: 'Enviando itens para cozinha!' });

                    $.ajax({
                        url		: base_url + 'pdv/enviar_pedido_mobile/' ,
                        type	: 'POST',
                        data	: {
                            pedido_mobile_id    : PEDIDO_ID,
                            pedido_id_gravado   : pedido_id_gravado,
                            mesa_id             : mesa_id,
                            idGarcom            : USUARIO,
                            itens               : JSON.stringify(itens),
                        },
                        cache	: true,
                        async 	: true
                    }).done(function(idPedido){

                        idPedido = idPedido.substring(0, 8);
                        idPedido = parseInt(idPedido);

                        banco.transaction(function(tx) {
                            dados = [idPedido, PEDIDO_ID];
                            tx.executeSql("UPDATE "+db_pedidos.nm_table()+" SET pedido_id = ?  WHERE id = ?", dados);
                        });

                        banco.transaction(function(tx) {
                                var enviado = 1;
                                dados = [enviado, PEDIDO_ID];
                                tx.executeSql("UPDATE "+db_pedido_item.nm_table()+" SET enviado = ?  WHERE pedido_mobile_id = ?", dados);
                            }, function () {},
                            function () {
                                db_pedidos.sincronizar_pedido(banco,idPedido, base_url);
                            });
                    });
                });
            });
        });
    },

    fechar_pedido: function (banco, base_url) {+

        banco.transaction(function(tx) {
            tx.executeSql("SELECT * FROM " + db_pedidos.nm_table() + " WHERE id = " + PEDIDO_ID + ";", [], function (tx, res) {
                for (var i = 0;i<res.rows.length;i++) {

                    pedido_id = res.rows.item(i).pedido_id;

                    $.ajax({
                        url		: base_url + 'pdv/verificar_fechamento_pedido_mobile/' ,
                        type	: 'POST',
                        data	: {
                            pedido_id : pedido_id,
                        },
                        cache	: true,
                        async 	: true
                    }).done(function(verificar){
                        if (verificar === "1") {
                            banco.transaction(function (tx) {
                                dados = ['0', null, 0, PEDIDO_ID];
                                $.each(mesas_all, function (i, item) {
                                    if (item.idPedidoAberto === parseInt(PEDIDO_ID)) {
                                        mesas_all[i].status                 = 0;
                                        mesas_all[i].idPedidoAberto         = null;
                                        mesas_all[i].idPedidoAbertoSistema  = null;
                                        db_mesas.buscarAll(banco);
                                    }
                                });

                                $.mobile.changePage("#mesas");
                                PEDIDO_ID = null;
                                tx.executeSql("UPDATE " + db_mesas.nm_table() + " SET status = ? , idPedidoAberto = ? , importada = ? WHERE idPedidoAberto = ?", dados);
                            });
                        } else {
                            alert('Mesa ainda não realizou o pagamento, encaminhe o cliente até o caixa para realizar o fechamento.');
                        }
                    });
                }
            });
        });
    },

    sincronizar_alfa: function (banco, base_url) {

        BANCO_ENGINER  = banco;

        db_mesas.fechar_mesa_importada(banco);

        $.ajax({
            url		: base_url + 'pdv/sincronizar_pedidos/' ,
            type	: 'POST',
            data	: {
                mesas_all: JSON.stringify(mesas_all),
            },
            cache	: true,
            async 	: true
        }).done(function(all_pedidos){

            var all_pedidos_split = all_pedidos.split("@arrays@");

            if (all_pedidos_split.length > 0) {

                pedidos_all_sincronizacao = all_pedidos_split[0];
                pedidos_all_sincronizacao = JSON.parse(pedidos_all_sincronizacao);

                pedidos_itens_all_sincronizacao = all_pedidos_split[1];
                pedidos_itens_all_sincronizacao = JSON.parse(pedidos_itens_all_sincronizacao);

                $.each(pedidos_all_sincronizacao, function (i, item) {

                    banco.transaction(function(tx) {

                        tx.executeSql("SELECT * FROM " + db_pedidos.nm_table() + " WHERE pedido_id = " + item.id_pedido, [], function (tx, res) {

                            var id_pedido = item.id_pedido;

                            if (res.rows.length === 0) {
                                var mesa_id     = item.id_mesa;
                                var nome_mesa   = item.nome_mesa
                                var pedido_save = [id_pedido, mesa_id, nome_mesa, 0 , 1 ];
                                db_pedidos.save_sincronizar(BANCO_ENGINER, pedido_save);
                            } else {
                                for (var i = 0;i<res.rows.length;i++) {
                                    var id_pedido_mobile = res.rows.item(i).id;
                                    db_pedidos.deleteAllItensImportadosByPedidoMobile(BANCO_ENGINER, id_pedido_mobile, id_pedido);
                                }
                            }
                        });

                    });
                });
            }

        });
    },

    sincronizar_pedido: function (banco, pedido_id, base_url) {

        BANCO_ENGINER  = banco;

        $.ajax({
            url		: base_url + 'pdv/sincronizar_pedidos/' ,
            type	: 'POST',
            data	: {
                pedido_id: pedido_id,
            },
            cache	: true,
            async 	: true
        }).done(function(all_pedidos){

            var all_pedidos_split = all_pedidos.split("@arrays@");

            if (all_pedidos_split.length > 0) {

                pedidos_all_sincronizacao = all_pedidos_split[0];
                pedidos_all_sincronizacao = JSON.parse(pedidos_all_sincronizacao);

                pedidos_itens_all_sincronizacao = all_pedidos_split[1];
                pedidos_itens_all_sincronizacao = JSON.parse(pedidos_itens_all_sincronizacao);

                $.each(pedidos_all_sincronizacao, function (i, item) {

                    banco.transaction(function(tx) {
                        tx.executeSql("SELECT * FROM " + db_pedidos.nm_table() + " WHERE pedido_id = " + item.id_pedido, [], function (tx, res) {

                            var id_pedido = item.id_pedido;
                            for (var i = 0;i<res.rows.length;i++) {
                                var id_pedido_mobile = res.rows.item(i).id;
                                db_pedidos.deleteAllItensImportadosByPedidoMobileViewPedido(BANCO_ENGINER, id_pedido_mobile, id_pedido);
                            }
                        });
                    });
                });
            }
        });
    },
 };