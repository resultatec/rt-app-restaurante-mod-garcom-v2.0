
var PEDIDO_ID = null;
var PEDIDO_SISTEMA_ID = null;
var MESA_ID_SELECIONADA = null;

function abrirMesa(tag) {
    if(confirm("Deseja realmente abrir esta mesa?")) {

        var mesa_id = tag.getAttribute('mesa_id');
        var nome_mesa = 'Mesa '+tag.getAttribute('mesa_nome');
        var pedido_save = ['', mesa_id ,nome_mesa, 0 , 0 ];

        db_pedidos.save(banco.getBancoDados(), pedido_save);

        $('.pedido_mesa_div').html('Pedido da Mesa ' +nome_mesa);

        $.mobile.changePage("#categorias");

        if (navigator.notification != undefined) {
            navigator.notification.vibrate(2000);
        }
    }
}

function adicionarQuantidadeItem(produto_id) {
    var tag = $('#quantidade_produto_id'+produto_id);
    var tagQuantidade = tag.val();
    tag.val(parseInt(tagQuantidade) + 1);
}

function adicionarQuantidadeItemEditar(produto_id) {
    var tag = $('#quantidade_produto_id_editar'+produto_id);
    tag.attr("editado",1);
    var tagQuantidade = tag.val();
    tag.val(parseInt(tagQuantidade) + 1);
}

function editarQuantidadeItemEditar(produto_id) {
    var tag = $('#quantidade_produto_id_editar'+produto_id);
    tag.attr("editado",1);
}

function enviar() {
    db_pedidos.enviar(banco.getBancoDados());
}

function fechar_pedido() {
    if (confirm("Deseja fechar o pedido?")) {
        db_pedidos.fechar_pedido(banco.getBancoDados(), base_url);
    }
}
